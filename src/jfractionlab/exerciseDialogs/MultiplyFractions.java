/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsRectangle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WannaReduceQuestion;
import jfractionlab.jflDialogs.WorkSheetDialog;





public class MultiplyFractions extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	
	private MyJTextField tf_numerator1 = new MyJTextField(2);
	private MyJTextField tf_numerator2 = new MyJTextField(2);
	private MyJTextField tf_calculation_numerator1 = new MyJTextField(2);
	private MyJTextField tf_calculation_numerator2 = new MyJTextField(2);
	private MyJTextField tf_reduced_numerator1 = new MyJTextField(2);
	private MyJTextField tf_reduced_numerator2 = new MyJTextField(2);
	private MyJTextField tf_endresult_numerator = new MyJTextField(2);

	private MyJTextField tf_denominator1 = new MyJTextField(2);
	private MyJTextField tf_denominator2 = new MyJTextField(2);
	private MyJTextField tf_calculation_denominator1 = new MyJTextField(2);
	private MyJTextField tf_calculation_denominator2 = new MyJTextField(2);
	private MyJTextField tf_reduced_denominator1 = new MyJTextField(2);
	private MyJTextField tf_reduced_denominator2 = new MyJTextField(2);
	private MyJTextField tf_endresult_denominator = new MyJTextField(2);
	
	//liefert den Zugang zum aktuellen im Krzungsprozess aktuellem TextFeld
	private MyJTextField tf_reductionprocess_outer_numerator;
	private MyJTextField tf_reductionprocess_outer_denominator;
	//liefert den Zugang zum aktuellen im Krzungsprozess aktuellem Label
	//, um den aktuellen werte lesen und schreiben zu koennen
	private MyJTextField tf_reductionprocess_inner_numerator;
	private MyJTextField tf_reductionprocess_inner_denominator;
	
	private FractionAsRectangle pizza1 = new FractionAsRectangle();
	private FractionAsRectangle pizza2 = new FractionAsRectangle();
	private FractionAsRectangle calculation_pizza = new FractionAsRectangle();
	private FractionAsRectangle endresult_pizza = new FractionAsRectangle();

	//Zahlen
	private FractionMaker brueche;
	private int numerator1;
	private int numerator2;
	private int denominator1;
	private int denominator2;

	
	private boolean bl_problem_was_solved_with_reducing = false;
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public MultiplyFractions(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("multiply_fracs"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;

		//Menu
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);
		jmOptions.addSeparator();
		
		rb_no_reducing.addActionListener(this);
		jmOptions.add(rb_no_reducing);
		rb_reducing.addActionListener(this);
		jmOptions.add(rb_reducing);
		jmOptions.addSeparator();
		
		rb_smallNb.addActionListener(this);
		jmOptions.add(rb_smallNb);
		rb_bigNb.addActionListener(this);
		jmOptions.add(rb_bigNb);
		
		jmb.add(jmOptions);
		
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
		
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double sizes[][] = {{
			// Spalten
			0.1,0.05,0.025,
			0.05,0.1,0.025,
			0.0885,0.05,0.05,
			0.05,0.0885,0.025,
			0.05,0.05,0.05,
			0.14,0.01
		},{
			//Zeilen
			TableLayout.FILL,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			TableLayout.FILL,
			0.60,
			30
		}};

		Container content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		//--Zaehler
			addTextField(tf_numerator1);
			tf_numerator1.addKeyListener(this);
			tf_numerator1.setEditable(false);
		content.add(tf_numerator1, "1,1,c,b");
			addTextField(tf_numerator2);
			tf_numerator2.addKeyListener(this);
			tf_numerator2.setEditable(false);
		content.add(tf_numerator2, "3,1,c,b");
		content.add(tf_calculation_numerator1, "7,1,r,b");
		content.add(tf_calculation_numerator2, "9,1,l,b");
			addTextField(tf_reduced_numerator1);
			tf_reduced_numerator1.addKeyListener(this);
			tf_reduced_numerator1.setEditable(false);
		content.add(tf_reduced_numerator1, "7,0,r,b");
			addTextField(tf_reduced_numerator2);
			tf_reduced_numerator2.addKeyListener(this);
			tf_reduced_numerator2.setEditable(false);
		content.add(tf_reduced_numerator2, "9,0,l,b");
			addTextField(tf_endresult_numerator);
			tf_endresult_numerator.addKeyListener(this);
			tf_endresult_numerator.setEditable(false);
		content.add(tf_endresult_numerator, "13,1,c,b");
		//--Nenner
			addTextField(tf_denominator1);
			tf_denominator1.addKeyListener(this);
			tf_denominator1.setEditable(false);
		content.add(tf_denominator1, "1,3,c,t");
			addTextField(tf_denominator2);
			tf_denominator2.addKeyListener(this);
			tf_denominator2.setEditable(false);
		content.add(tf_denominator2, "3,3,c,t");
		content.add(tf_calculation_denominator1, "7,3,r,t");
		content.add(tf_calculation_denominator2, "9,3,l,t");
			addTextField(tf_reduced_denominator1);
			tf_reduced_denominator1.addKeyListener(this);
			tf_reduced_denominator1.setEditable(false);
			content.add(tf_reduced_denominator1, "7,4,r,t");
			addTextField(tf_reduced_denominator2);
			tf_reduced_denominator2.addKeyListener(this);
			tf_reduced_denominator2.setEditable(false);
			content.add(tf_reduced_denominator2, "9,4,l,t");
			addTextField(tf_endresult_denominator);
			tf_endresult_denominator.addKeyListener(this);
			tf_endresult_denominator.setEditable(false);
		content.add(tf_endresult_denominator, "13,3,c,t");

		//--Zeichen
		JLabel lb_aufgabenMal = new JLabel("*", JLabel.CENTER);
		content.add(lb_aufgabenMal, "2,2");
		JLabel lb_calculationsMal1 = new JLabel("*", JLabel.CENTER);
		content.add(lb_calculationsMal1, "8,1,c,b");
		JLabel lb_calculationsMal2 = new JLabel("*", JLabel.CENTER);
		content.add(lb_calculationsMal2, "8,3,c,t");
		JLabel lb_gleich1 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich1, "5,2");
		JLabel lb_gleich2 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich2, "11,2");
		FractionLine bs_fraction1 = new FractionLine();
		content.add(bs_fraction1, "1,2");
		FractionLine bs_fraction2 = new FractionLine();
		content.add(bs_fraction2, "3,2");
		FractionLine bs_fraction3 = new FractionLine();
		content.add(bs_fraction3, "7,2,9,2");
		FractionLine bs_fraction4 = new FractionLine();
		content.add(bs_fraction4, "13,2");
		//--Pizzen
		content.add(pizza1, "0,5,1,5");
		content.add(pizza2, "3,5,4,5");
		
		content.add(calculation_pizza, "7,5,9,5");
		content.add(endresult_pizza, "12,5,14,5");

		btn_continue.addActionListener(this);
		btn_continue.addKeyListener(this);
		btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "15,1,c,b");
		btn_end.addActionListener(this);
		btn_end.addKeyListener(this);
		content.add(btn_end, "15,3,c,b");
		content.add(pdsp, "15,4,15,5");
		points = owner.points_multiplyFractions;
		pdsp.setText(String.valueOf(points));
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,6,15,6");
		makeProblem();
		String[] ar_howto = {
			"howto_nb_and_enter","howto_option_type_of_exercise", "howto_option_reducing"
		};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		if (smallNumbers){
			brueche = new FractionMaker(9);//der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
		}else{
			brueche = new FractionMaker(17);
		}
		numerator1 = brueche.getNumerator_1();
		denominator1 = brueche.getDenominator_1();
		numerator2 = brueche.getNumerator_2();
		denominator2 = brueche.getDenominator_2();
		//Testzahlen
		//numerator1 = 6;
		//denominator1 = 9;
		//numerator2 = 5;
		//denominator2 = 8;
		//first fraction
		tf_numerator1.setText(String.valueOf(numerator1));
		tf_denominator1.setText(String.valueOf(denominator1));
		pizza1.drawPizzaAsRectangle(numerator1, denominator1, Color.yellow, false,true);

		//second fraction
		tf_numerator2.setText(String.valueOf(numerator2));
		tf_denominator2.setText(String.valueOf(denominator2));
		pizza2.drawPizzaAsRectangle(numerator2, denominator2, Color.yellow, true,true);
		//calculation
		tf_calculation_numerator1.setText(String.valueOf(numerator1));
		tf_calculation_denominator1.setText(String.valueOf(denominator1));
		calculation_pizza.drawPizzaAsRectangle(numerator1, denominator1, numerator2, denominator2, Color.yellow,true);
		//second fraction of calculation
		tf_calculation_numerator2.setText(String.valueOf(numerator2));
		tf_calculation_denominator2.setText(String.valueOf(denominator2));
		//isReducable?
		if(bl_with_reducing){
			checkReducability();
		}else{
			
			//x x x
			if(
				JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1
				||
				JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1
				||
				JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1
				||
				JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1
			){
				new WannaReduceQuestion(this);
			}
			
			if(bl_wannaReduceQuestion_AnswerIsYes){
				bl_wannaReduceQuestion_AnswerIsYes = false;
				checkReducability();
			}else{
				tf_endresult_numerator.setEditable(true);
				tf_endresult_numerator.requestFocusInWindow();
			}
		}
	}
	
	/**
	 * 
	 *
	 */
	private void checkReducability(){
		if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1){
			initReductionProcess(1,1);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1){
			initReductionProcess(1,2);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1){
			initReductionProcess(2,1);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1){
			initReductionProcess(2,2);
		}else{
			//System.out.println("checkReducability(): nichts ist teilbar");
			tf_endresult_numerator.setEditable(true);
			tf_endresult_numerator.requestFocusInWindow();
		}
	}//public void checkReducability()

	/**
	 * 
	 * @param z
	 * @param n
	 */
	private void initReductionProcess(int z, int n){
		bl_problem_was_solved_with_reducing = true;
		//System.out.println("initReductionProcesslProcess("+ z+","+n+")");
		if (z == 1){
			//System.out.println("initReduceProcess z==1");
			tf_reductionprocess_inner_numerator = tf_calculation_numerator1;
			tf_reductionprocess_outer_numerator = tf_reduced_numerator1;
		}else if(z == 2){
			//System.out.println("initReduceProcess z==2");
			tf_reductionprocess_inner_numerator = tf_calculation_numerator2;
			tf_reductionprocess_outer_numerator = tf_reduced_numerator2;
		}else{
			//????
		}
		if (n == 1){
			//System.out.println("initReduceProcess n==1");
			tf_reductionprocess_inner_denominator = tf_calculation_denominator1;
			tf_reductionprocess_outer_denominator = tf_reduced_denominator1;
		}else if(n == 2){
			//System.out.println("initReduceProcess n==2");
			tf_reductionprocess_inner_denominator = tf_calculation_denominator2;
			tf_reductionprocess_outer_denominator = tf_reduced_denominator2;
		}else{
			//???
		}
		tf_reductionprocess_outer_denominator.setEditable(true);
		tf_reductionprocess_outer_numerator.setEditable(true);
		tf_reductionprocess_outer_numerator.requestFocusInWindow();
	}//initReductionProcess

	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		clearTextFields();
		calculation_pizza.noPizzaAsRectangle();
		endresult_pizza.noPizzaAsRectangle();
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			pizza1.noPizzaAsRectangle();
			pizza2.noPizzaAsRectangle();
			tf_numerator1.setText("");
			tf_numerator1.setEditable(true);
			tf_numerator1.requestFocusInWindow();
		}
	}//public nextProblem

	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if (obj == rb_reducing){
			//System.out.println("rb_reducing");
			//the program is in "no reduce mode" actually
			//it should switch to "reduce-mode"
			bl_with_reducing = true;
			nextProblem();
		}else if (obj == rb_no_reducing){
			//System.out.println("rb_no_reucing");
			//the program is in "reduce mode" actually
			//it should switch to "no reduce-mode"
			bl_with_reducing = false;
			nextProblem();
		}else if (obj == rb_smallNb){
			// the system is in "big Numbers-Mode" actually
			// it should switch to "small Numbers Mode"
			smallNumbers = true;
			nextProblem();
		}else if (obj == rb_bigNb){
			// the system is in "small Numbers Mode"
			// it should switch to "big numbers mode"
			smallNumbers = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.MULTIPLY_FRACTIONS,
					lang.Messages.getString("multiply_fractions"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"multiply"
			);
		}
	}//actionPerformed

	/**
	 * 
	 */
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}else if(obj == tf_numerator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator1) != null){
				numerator1 = Integer.parseInt(tf_numerator1.getText());
				tf_numerator1.setEditable(false);
				tf_denominator1.setEditable(true);
				tf_denominator1.requestFocusInWindow();
			}//if(readInputNumber(tf_numerator1)
		}else if(obj == tf_denominator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator1) != null){
				int pn = Integer.parseInt(tf_denominator1.getText());
				if(pn <= numerator1){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_denominator1.setText("");
					tf_denominator1.setEditable(false);
					tf_numerator1.setText("");
					tf_numerator1.setEditable(true);
					tf_numerator1.requestFocusInWindow();
				}else{
					denominator1 = pn;
					tf_denominator1.setEditable(false);
					pizza1.drawPizzaAsRectangle(numerator1, denominator1, Color.yellow, false,true);
					tf_numerator2.setEditable(true);
					tf_numerator2.requestFocusInWindow();
				}
			}//if(readInputNumber(tf_denominator1)
		}else if(obj == tf_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator2) != null){
				numerator2 = Integer.parseInt(tf_numerator2.getText());
				tf_numerator2.setEditable(false);
				tf_denominator2.setEditable(true);
				tf_denominator2.requestFocusInWindow();
			}//if(readInputNumber(tf_numerator2)
		}else if(obj == tf_denominator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator2) != null){
				int pn = Integer.parseInt(tf_denominator2.getText());
				if(pn <= numerator2){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_denominator2.setText("");
					tf_denominator2.setEditable(false);
					tf_numerator2.setText("");
					tf_numerator2.setEditable(true);
					tf_numerator2.requestFocusInWindow();
				}else{
					denominator2 = pn;
					tf_denominator2.setEditable(false);
					pizza2.drawPizzaAsRectangle(numerator2, denominator2, Color.yellow, true,true);
					tf_calculation_numerator1.setText(String.valueOf(numerator1));
					tf_calculation_denominator1.setText(String.valueOf(denominator1));
					calculation_pizza.drawPizzaAsRectangle(numerator1, denominator1, numerator2, denominator2, Color.yellow,true);
					tf_calculation_numerator2.setText(String.valueOf(numerator2));
					tf_calculation_denominator2.setText(String.valueOf(denominator2));
					if(bl_with_reducing){
						checkReducability();
					}else{
						tf_endresult_numerator.setEditable(true);
						tf_endresult_numerator.requestFocusInWindow();
					}
				}
			}//if(readInputNumber(tf_denominator2)
		}else if(obj == tf_reduced_numerator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_numerator1) != null){
					tf_reductionprocess_outer_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_reduced_denominator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_denominator1) != null){
					tf_reduceprocess_denominator_Action();
			}
		}else if(obj == tf_reduced_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_numerator2) != null){
					tf_reductionprocess_outer_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_reduced_denominator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_denominator2) != null){
				int z = Integer.parseInt(tf_reduced_denominator2.getText());
				if (z == 0){
					tf_reduced_denominator2.setText("");
					lb_info.setText(lang.Messages.getString("no_null"));
				}else{
					tf_reduceprocess_denominator_Action();
				}
			}
		}else if(obj == tf_endresult_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_endresult_numerator) != null){
				int z1 = Integer.parseInt(tf_calculation_numerator1.getText());
				int z2 = Integer.parseInt(tf_calculation_numerator2.getText());
				int zz = Integer.parseInt(tf_endresult_numerator.getText());
				//System.out.println("focusLost obj == tf_endresult_numerator -- try");
				//System.out.println(z1+", "+z2+", "+zz);
				if(z1 * z2 != zz){
					lb_info.setText(lang.Messages.getString("multiply_nums"));
					tf_endresult_numerator.setText("");
				}else{
					tf_endresult_numerator.setEditable(false);
					tf_endresult_denominator.setEditable(true);
					tf_endresult_denominator.requestFocusInWindow();
				}
			}
		}else if(obj == tf_endresult_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_endresult_denominator) != null){
				int cn1 = Integer.parseInt(tf_calculation_numerator1.getText());
				int cn2 = Integer.parseInt(tf_calculation_numerator2.getText());
				int cd1 = Integer.parseInt(tf_calculation_denominator1.getText()); 
				int cd2 = Integer.parseInt(tf_calculation_denominator2.getText());
				int ez  = Integer.parseInt(tf_endresult_numerator.getText());
				int en  = Integer.parseInt(tf_endresult_denominator.getText());
				if(cd1 * cd2 != en){
					lb_info.setText(lang.Messages.getString("multiply_denomis"));
					tf_endresult_denominator.setText("");
				}else{
					tf_endresult_denominator.setEditable(false);
					if(cn1 < cd1 && cn2 < cd2){
						endresult_pizza.drawPizzaAsRectangle(cn1, cd1, cn2, cd2, Color.yellow,true);
					}else{
						endresult_pizza.drawPizzaAsRectangle(ez, en, Color.yellow, false,true);
					}
					if(bl_randomProblem){
						points++;
						if(bl_problem_was_solved_with_reducing){
							bl_problem_was_solved_with_reducing = false;
							points++;
						}
					}
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "multiplyFractions");
					lb_info.setText(lang.Messages.getString("that_is_right"));
					btn_continue.requestFocusInWindow();
				}
			}
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}

	/**
	 * 
	 * @param n
	 * @param nn
	 * @return
	 */
	private boolean inRow(int n, int nn){
		boolean wert = false;
		if (nn>n){
			if(nn%n == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn<n){
			if(n%nn == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn == n){ wert =  true;}
		return wert;
	}//inRow(int n, int nn)
		
	/**
	 * 
	 *
	 */
	private void tf_reduceprocess_denominator_Action(){
		//System.out.println("tf_reduceprocess_denominator_Action()");
		int z=0; int z_neu = 0; int n = 0; int n_neu = 0;
		boolean lbnbOK = true;
		try{
			z = Integer.parseInt(tf_reductionprocess_inner_numerator.getText());
			n = Integer.parseInt(tf_reductionprocess_inner_denominator.getText());
		}catch(NumberFormatException nfe){
			nfe.printStackTrace();
			lb_info.setText("dieser fehler sollte eigentlich unmoeglich sein!");
			lbnbOK = false;
		}
		if(readInputInt(tf_reductionprocess_outer_denominator) != null && lbnbOK){
			z_neu = Integer.parseInt(tf_reductionprocess_outer_numerator.getText());
			n_neu = Integer.parseInt(tf_reductionprocess_outer_denominator.getText());
			//System.out.println("tf_reduceprocess_denominator_FocusLost: z/z_neu/n/n_neu"+z+"/"+z_neu+"/"+n+"/"+n_neu);
			//sind z o n vielfache oder teiler??
			if(inRow(z,z_neu) && inRow(n,n_neu)){
				if(JFractionLab.greatestCommonDivisor(z_neu,n_neu) != 1 || z/z_neu != n/n_neu){ //Es kann weitergekrzt werden
					if(z/z_neu != n/n_neu){//reduction is wrong
						lb_info.setText(lang.Messages.getString("div_by_same_factor"));
					}else{//reduction is right but not optimal
						lb_info.setText(lang.Messages.getString("reduce_better"));
					}
					tf_reductionprocess_outer_denominator.setText("");
					tf_reductionprocess_outer_numerator.setEditable(true);
					tf_reductionprocess_outer_numerator.requestFocusInWindow();
				}else{//Es kann nicht weitergekrzt werden
					//System.out.println("Es kann nicht weitergekrzt werden ??");
					tf_reductionprocess_outer_numerator.setEditable(false);
					tf_reductionprocess_outer_denominator.setEditable(false);
					tf_reductionprocess_outer_numerator.setText("");
					tf_reductionprocess_outer_denominator.setText("");
					tf_reductionprocess_inner_numerator.setText(String.valueOf(z_neu));
					tf_reductionprocess_inner_denominator.setText(String.valueOf(n_neu));
					//System.out.println("neue werte:"+tf_calculation_numerator1.getText());
					//System.out.println("neue werte:"+tf_calculation_denominator1.getText());
					checkReducability();
				}//if (JFractionLab.ggt(z,n) != 1)
			}else{//sind nicht in Reihe
				//System.out.println("Eine der Zahlen ist keine Vielfache!!");
				tf_reductionprocess_outer_numerator.setText("");
				tf_reductionprocess_outer_numerator.requestFocusInWindow();
				tf_reductionprocess_outer_denominator.setText("");
				lb_info.setText(lang.Messages.getString("div_n_and_dn_by_same_nb"));
			}//if in reihe
		}
	}
}//class
