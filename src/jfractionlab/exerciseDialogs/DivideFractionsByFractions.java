/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsRectangle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;





public class DivideFractionsByFractions extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private MyJTextField tf_divident_numerator = new MyJTextField(2);
	private MyJTextField tf_divisor_numerator = new MyJTextField(2);
	private MyJTextField tf_calculation_numerator1 = new MyJTextField(2);
	private MyJTextField tf_calculation_numerator2 = new MyJTextField(2);
	private MyJTextField tf_reduced_numerator1 = new MyJTextField(2);
	private MyJTextField tf_reduced_numerator2 = new MyJTextField(2);
	private MyJTextField tf_provisionalresult_numerator = new MyJTextField(2);

	private MyJTextField tf_divident_denominator = new MyJTextField(2);
	private MyJTextField tf_divisor_denominator = new MyJTextField(2);
	private MyJTextField tf_calculation_denominator1 = new MyJTextField(2);
	private MyJTextField tf_calculation_denominator2 = new MyJTextField(2);
	private MyJTextField tf_reduced_denominator1 = new MyJTextField(2);
	private MyJTextField tf_reduced_denominator2 = new MyJTextField(2);
	private MyJTextField tf_provisionalresult_denominator = new MyJTextField(2);

	private MyJTextField tf_endresult_integer = new MyJTextField(2);
	private MyJTextField tf_endresult_numerator = new MyJTextField(2);
	private MyJTextField tf_endresult_denominator = new MyJTextField(2);
	//"MetaTextFields"
	//bietet Zugang zu dem "inneren", im Krzungsprozess aktuellem Textfeld
	private MyJTextField tf_reduce_inner_numerator;
	private MyJTextField tf_reduce_inner_denominator;
	//bietet Zugang zu dem "��ren", im Krzungsprozess aktuellem Textfeld
	private MyJTextField tf_reduce_outer_numerator;
	private MyJTextField tf_reduce_outer_denominator;
	
	private JLabel lb_question = new JLabel("", JLabel.CENTER);
	private FractionAsRectangle dividend_pizza = new FractionAsRectangle();
	private FractionAsRectangle divisor_pizza = new FractionAsRectangle();
	private FractionAsRectangle calculation_pizza = new FractionAsRectangle();
	private FractionAsRectangle provisionalresult_pizza = new FractionAsRectangle();

	//Zahlen
	private FractionMaker fractions;
	private int divident_numerator;
	private int divident_denominator;
	private int divisor_numerator;
	private int divisor_denominator;
	private int provisionalresult_numerator;
	private int provisionalresult_denominator;
	private int result_integer;
	private boolean result_is_integer = false;
	
	
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public DivideFractionsByFractions(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("divide_the_fracs"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		//setModal(true); //?? Wenn auskommentiert, gibt es keine Zahlen auf der GUI????
		this.owner = owner;

		//Menu
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);

		jmb.add(jmOptions);
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);

		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double schmal = 0.03;
		double normal = 0.06;
		double breit = 0.09;
        	double sizes[][] = {{
			// Spalten
			breit,schmal,
			breit,schmal,
			schmal,normal,schmal, 
			normal,schmal,schmal, 
			breit,
			schmal, breit,
			normal, schmal,
			TableLayout.FILL
		},{
			//Zeilen
			TableLayout.FILL,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			TableLayout.FILL,
			0.10,
			0.50,
			30
		}};
		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		//--Zaehler
			addTextField(tf_divident_numerator);
			tf_divident_numerator.addKeyListener(this);
			tf_divident_numerator.setEditable(false);
		content.add(tf_divident_numerator, "0,1,c,b");
			addTextField(tf_divisor_numerator);
			tf_divisor_numerator.addKeyListener(this);
			tf_divisor_numerator.setEditable(false);
		content.add(tf_divisor_numerator, "2,1,c,b");
			addTextField(tf_calculation_numerator1);
			tf_calculation_numerator1.setEditable(false);
		content.add(tf_calculation_numerator1, "5,1,r,b");
			addTextField(tf_calculation_numerator2);
			tf_calculation_numerator2.setEditable(false);
			tf_calculation_numerator2.addKeyListener(this);
		content.add(tf_calculation_numerator2, "7,1,l,b");
			addTextField(tf_reduced_numerator1);
			tf_reduced_numerator1.addKeyListener(this);
			tf_reduced_numerator1.setEditable(false);
		content.add(tf_reduced_numerator1, "5,0,r,b");
			addTextField(tf_reduced_numerator2);
			tf_reduced_numerator2.addKeyListener(this);
			tf_reduced_numerator2.setEditable(false);
		content.add(tf_reduced_numerator2, "7,0,l,b");
			addTextField(tf_provisionalresult_numerator);
			tf_provisionalresult_numerator.addKeyListener(this);
			tf_provisionalresult_numerator.setEditable(false);
		content.add(tf_provisionalresult_numerator, "10,1,c,b");
			addTextField(tf_endresult_numerator);
			tf_endresult_numerator.addKeyListener(this);
			tf_endresult_numerator.setEditable(false);
		content.add(tf_endresult_numerator, "13,1,c,b");
		//--Nenner
			addTextField(tf_divident_denominator);
			tf_divident_denominator.addKeyListener(this);
			tf_divident_denominator.setEditable(false);
		content.add(tf_divident_denominator, "0,3,c,t");
			addTextField(tf_divisor_denominator);
			tf_divisor_denominator.addKeyListener(this);
			tf_divisor_denominator.setEditable(false);
		content.add(tf_divisor_denominator, "2,3,c,t");
			addTextField(tf_calculation_denominator1);
			tf_calculation_denominator1.setEditable(false);
		content.add(tf_calculation_denominator1, "5,3,r,t");
			addTextField(tf_calculation_denominator2);
			tf_calculation_denominator2.addKeyListener(this);
			tf_calculation_denominator2.setEditable(false);
		content.add(tf_calculation_denominator2, "7,3,l,t");
			addTextField(tf_reduced_denominator1);
			tf_reduced_denominator1.addKeyListener(this);
			tf_reduced_denominator1.setEditable(false);
		content.add(tf_reduced_denominator1, "5,4,r,t");
			addTextField(tf_reduced_denominator2);
			tf_reduced_denominator2.addKeyListener(this);
			tf_reduced_denominator2.setEditable(false);
		content.add(tf_reduced_denominator2, "7,4,l,t");
			addTextField(tf_provisionalresult_denominator);
			tf_provisionalresult_denominator.addKeyListener(this);
			tf_provisionalresult_denominator.setEditable(false);
		content.add(tf_provisionalresult_denominator, "10,3,c,t");
			addTextField(tf_endresult_denominator);
			tf_endresult_denominator.addKeyListener(this);
			tf_endresult_denominator.setEditable(false);
		content.add(tf_endresult_denominator, "13,3,c,t");
			addTextField(tf_endresult_integer);
			tf_endresult_integer.addKeyListener(this);
			tf_endresult_integer.setEditable(false);
		content.add(tf_endresult_integer, "12,1,12,3,c,c");
		
		content.add(lb_question, "0,5,15,5,c,c");
		
		//--Zeichen
		JLabel lb_geteiltzeichen = new JLabel(":", JLabel.CENTER);
		content.add(lb_geteiltzeichen, "1,2");
			
		JLabel lb_calculationsMal1 = new JLabel("*", JLabel.CENTER);
		content.add(lb_calculationsMal1, "6,1,c,b");
		JLabel lb_calculationsMal2 = new JLabel("*", JLabel.CENTER);
		content.add(lb_calculationsMal2, "6,3,c,t");
		JLabel lb_gleich1 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich1, "3,2");
		
		JLabel lb_gleich2 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich2, "9,2");
		
		JLabel lb_gleich3 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich3, "11,2");

		
		FractionLine bs_fraction1 = new FractionLine();
		content.add(bs_fraction1, "0,2");
		FractionLine bs_fraction2 = new FractionLine();
		content.add(bs_fraction2, "2,2");
		FractionLine bs_fraction3 = new FractionLine();
		content.add(bs_fraction3, "5,2,7,2");
		FractionLine bs_fraction4 = new FractionLine();
        	content.add(bs_fraction4, "10,2");
		FractionLine bs_fraction5 = new FractionLine();
        	content.add(bs_fraction5, "13,2");
		//--Pizzen
		content.add(dividend_pizza, "0,6,1,6");
		content.add(divisor_pizza, "2,6,3,6");
		//content.add(calculation_pizza, "7,6,9,6");
		//content.add(zwischenendresult_pizza, "12,6,14,6");

		btn_continue.addActionListener(this);
		btn_continue.addKeyListener(this);
		btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "15,1,c,b");
		btn_end.addActionListener(this);
		btn_end.addKeyListener(this);
		content.add(btn_end, "15,3,c,b");
		content.add(pdsp, "15,4,15,6");
		points = owner.points_divideFractionsByFractions;
		pdsp.setText(String.valueOf(points));
		
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,7,15,7");
		

		makeProblem();
		String[] ar_howto = {
			"howto_nb_and_enter","howto_option_type_of_exercise"
		};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		
		fractions = new FractionMaker(10); //der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
		divident_numerator = fractions.getNumerator_1();
		divident_denominator = fractions.getDenominator_1();
		divisor_numerator = fractions.getNumerator_2();
		divisor_denominator = fractions.getDenominator_2();
		
		//Testzahlen
		//divident_numerator = 3;//(6/9 : 2/3 = 1!)
		//divident_denominator = 6;//4/6 : 8/9
		//divisor_numerator = 2;//7/8 : 4/7
		//divisor_denominator = 9;
		
		tf_divident_numerator.setText(String.valueOf(divident_numerator));
		tf_divident_denominator.setText(String.valueOf(divident_denominator));
		tf_divisor_numerator.setText(String.valueOf(divisor_numerator));
		tf_divisor_denominator.setText(String.valueOf(divisor_denominator));
		
		tf_calculation_numerator1.setText(String.valueOf(divident_numerator));
		tf_calculation_denominator1.setText(String.valueOf(divident_denominator));
		tf_calculation_numerator2.setEditable(true);
		tf_calculation_numerator2.requestFocusInWindow();
		lb_question.setText(lang.Messages.getString("how_often_fits_the_right_in_the_left"));
		
		dividend_pizza.drawPizzaAsRectangle(divident_numerator, divident_denominator, Color.yellow, false,true);
		divisor_pizza.drawPizzaAsRectangle(divisor_numerator, divisor_denominator, Color.yellow, false,true);
	}//maleProblem
	
	/**
	 * 
	 *
	 */
 	private void checkReducability(){
		if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1){
			initReductionProcess(1,1);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1){
			initReductionProcess(1,2);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator1.getText())) != 1){
			initReductionProcess(2,1);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator2.getText())) != 1){
			initReductionProcess(2,2);
		}else{
			//System.out.println("checkReducability(): nichts ist teilbar");
			tf_provisionalresult_numerator.setEditable(true);
			tf_provisionalresult_numerator.requestFocusInWindow();
			//tf_zwischenergebnis_numerator.requestFocusInWindow();
		}
	}//public void checkReducability()

 	/**
 	 * 
 	 * @param z
 	 * @param n
 	 */
	private void initReductionProcess(int z, int n){
		//System.out.println("initReductionProcess("+ z+","+n+")");
		if (z == 1){
			tf_reduce_inner_numerator = tf_calculation_numerator1;
			tf_reduce_outer_numerator = tf_reduced_numerator1;
		}else if(z == 2){
			tf_reduce_inner_numerator = tf_calculation_numerator2;
			tf_reduce_outer_numerator = tf_reduced_numerator2;
		}else{
			//????
		}
		if (n == 1){
			tf_reduce_inner_denominator = tf_calculation_denominator1;
			tf_reduce_outer_denominator = tf_reduced_denominator1;
		}else if(n == 2){
			tf_reduce_inner_denominator = tf_calculation_denominator2;
			tf_reduce_outer_denominator = tf_reduced_denominator2;
		}else{
			//???
		}
		tf_reduce_outer_denominator.setEditable(true);
		tf_reduce_outer_numerator.setEditable(true);
		tf_reduce_outer_numerator.requestFocusInWindow();
	}//initReductionProcess

	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		clearTextFields();
		dividend_pizza.noPizzaAsRectangle();
		calculation_pizza.noPizzaAsRectangle();
		provisionalresult_pizza.noPizzaAsRectangle();
		result_is_integer = false;
		
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			tf_divident_numerator.setEditable(true);
			tf_divident_numerator.requestFocusInWindow();
			divisor_pizza.noPizzaAsRectangle();
		}//else
	}//public void nextProblem()

	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
        if (obj == btn_continue){
        	nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.DIVIDE_FRACTIONS,
					lang.Messages.getString("div_fr_by_fr"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"divide-by-fractions"
			);
		}
	}//actionPerformed
	
	/**
	 * 
	 */
	public void keyPressed(KeyEvent e){
		Object obj = e.getSource();
		int key = e.getKeyCode();
		lb_info.setText("");
		if(obj == tf_divident_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_divident_numerator) != null){
				divident_numerator = Integer.parseInt(tf_divident_numerator.getText());
				tf_divident_numerator.setEditable(false);
				tf_divident_denominator.setEditable(true);
				tf_divident_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_divident_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_divident_denominator) != null){
				int pn = Integer.parseInt(tf_divident_denominator.getText());
				if(pn <= divident_numerator){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_divident_denominator.setText("");
					tf_divident_denominator.setEditable(false);
					tf_divident_numerator.setText("");
					tf_divident_numerator.setEditable(true);
					tf_divident_numerator.requestFocusInWindow();
				}else{
					divident_denominator = pn;
					tf_divident_denominator.setEditable(false);
					dividend_pizza.drawPizzaAsRectangle(divident_numerator, divident_denominator, Color.yellow, false,true);
					tf_divisor_numerator.setEditable(true);
					tf_calculation_numerator1.setText(String.valueOf(divident_numerator));
					tf_calculation_denominator1.setText(String.valueOf(divident_denominator));
					tf_divisor_numerator.requestFocusInWindow();
				}
			}
		}else if(obj == tf_divisor_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_divisor_numerator) != null){
				divisor_numerator = Integer.parseInt(tf_divisor_numerator.getText());
				tf_divisor_numerator.setEditable(false);
				tf_divisor_denominator.setEditable(true);
				tf_divisor_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_divisor_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_divisor_denominator) != null){
				int pn = Integer.parseInt(tf_divisor_denominator.getText());
				if (pn <= divisor_numerator){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_divisor_denominator.setText("");
					tf_divisor_denominator.setEditable(false);
					tf_divisor_numerator.setText("");
					tf_divisor_numerator.setEditable(true);
					tf_divisor_numerator.requestFocusInWindow();
				}else{
					divisor_denominator = pn;
					tf_divisor_denominator.setEditable(false);
					divisor_pizza.drawPizzaAsRectangle(divisor_numerator, divisor_denominator, Color.yellow, false,true);
					tf_calculation_numerator2.setEditable(true);
					tf_calculation_numerator2.requestFocusInWindow();
				}
			}
		}else if(obj == tf_calculation_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_calculation_numerator2) != null){
				int z1 = Integer.parseInt(tf_calculation_numerator2.getText());
				if (z1 != divisor_denominator){
					lb_info.setText(lang.Messages.getString("mk_reciprocal"));
					tf_calculation_numerator2.setText("");
				}else{
					//System.out.println("focusLost: Zaehler ist OK : ");
					tf_calculation_numerator2.setEditable(false);
					tf_calculation_denominator2.setEditable(true);
					tf_calculation_denominator2.requestFocusInWindow();
				}
			}
		}else if(obj == tf_calculation_denominator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_calculation_denominator2) != null){
				int z1 = Integer.parseInt(tf_calculation_denominator2.getText());
				if (z1 != divisor_numerator){
					lb_info.setText(lang.Messages.getString("mk_reciprocal"));
					tf_calculation_denominator2.setText("");
				}else{
					tf_calculation_denominator2.setEditable(false);
					checkReducability();
				}
			}	
		}else if(obj == tf_reduced_numerator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_numerator1)!=null){
				tf_reduce_outer_denominator.setEditable(true);
				tf_reduce_outer_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_reduced_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_numerator2)!=null){
				tf_reduce_outer_denominator.setEditable(true);
				tf_reduce_outer_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_reduced_denominator1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_denominator1)!=null){
				tf_reduce_outer_denominator_FocusLost();
			}
		}else if(obj == tf_reduced_denominator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_reduced_denominator2)!=null){
				tf_reduce_outer_denominator_FocusLost();
			}
		}else if(obj == tf_provisionalresult_numerator & key == KeyEvent.VK_ENTER){
			//System.out.println("focusLost obj == tf_zwischenergebnis_numerator");
			if(readInputInt(tf_provisionalresult_numerator)!=null){
				int z1 = Integer.parseInt(tf_calculation_numerator1.getText());
				int z2 = Integer.parseInt(tf_calculation_numerator2.getText());
				int zz = Integer.parseInt(tf_provisionalresult_numerator.getText());
				//System.out.println("focusLost obj == tf_zwischenergebnis_numerator -- try");
				//System.out.println(z1+", "+z2+", "+zz);
				if(z1* z2 != zz){
					lb_info.setText(lang.Messages.getString("multiply_nums"));
					tf_provisionalresult_numerator.setText("");
				}else{
					provisionalresult_numerator = zz;
					tf_provisionalresult_numerator.setEditable(false);
					tf_provisionalresult_denominator.setEditable(true);
					tf_provisionalresult_denominator.requestFocusInWindow();
				}
			}
		}else if(obj == tf_provisionalresult_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_provisionalresult_denominator)!=null){
				int cd1 = Integer.parseInt(tf_calculation_denominator1.getText());
				int cd2 = Integer.parseInt(tf_calculation_denominator2.getText());
				int zz  = Integer.parseInt(tf_provisionalresult_denominator.getText());
				if(zz!= cd1*cd2){
					lb_info.setText(lang.Messages.getString("multiply_denomis"));
					tf_provisionalresult_denominator.setText("");
				}else{
					provisionalresult_denominator = zz;
					tf_provisionalresult_denominator.setEditable(false);
					if(provisionalresult_denominator > provisionalresult_numerator){
						//System.out.println("Ergebnis ist kleiner als 1 - fertig");
						if(bl_randomProblem){points++;};
						pdsp.setText(String.valueOf(points));
						owner.setPoints(points, "divideFractionsByFractions");
						lb_info.setText(lang.Messages.getString("that_is_right"));
						btn_continue.requestFocusInWindow();
					}else{
						//System.out.println("!!!!!zwischen_numerator%zwischen_denominator =
						//"+zwischenergebnis_numerator%zwischenergebnis_denominator);
						if (provisionalresult_numerator%provisionalresult_denominator == 0){
							//System.out.println("ergebnis_ist_glatt = true");
							result_is_integer = true;
						}else{
							//System.out.println("ergebnis_ist_glatt = false");
							result_is_integer = false;
						}
						tf_endresult_integer.setEditable(true);
						tf_endresult_integer.requestFocusInWindow();
					}
				}//(zz!= cd1*cd2)
			}
		}else if(obj == tf_endresult_integer & key == KeyEvent.VK_ENTER){
			//System.out.println("focusLost (tf_endresult_ganzzahl)");
			if(readInputInt(tf_endresult_integer)!=null){
				int zz = Integer.parseInt(tf_endresult_integer.getText());
				//System.out.println("zwischen_numerator/zwischen_denominator = "+zwischenergebnis_numerator/zwischenergebnis_denominator);
				if(zz == provisionalresult_numerator/provisionalresult_denominator){
					result_integer = zz;
					tf_endresult_integer.setEditable(false);
					if(result_is_integer){
						//System.out.println("if(ergebnis_ist_glatt) true :-)");
						if(bl_randomProblem){points++;};
						pdsp.setText(String.valueOf(points));
						owner.setPoints(points, "divideFractionsByFractions");
						lb_info.setText(lang.Messages.getString("that_is_right"));
						btn_continue.requestFocusInWindow();
					}else{
						tf_endresult_numerator.setEditable(true);
						tf_endresult_numerator.requestFocusInWindow();
					}
				}else{
					lb_info.setText(lang.Messages.getString("how_often_fits_the_denominator_in_the_numerator"));
					tf_endresult_integer.setText("");
				}
			}
		}else if(obj == tf_endresult_numerator & key == KeyEvent.VK_ENTER){
			//System.out.println("focusLost (tf_endresult_numerator)");
			if(readInputInt(tf_endresult_numerator)!=null){
				int zz = Integer.parseInt(tf_endresult_numerator.getText());
				int bla = provisionalresult_numerator - result_integer * provisionalresult_denominator;
				if(zz == bla){
					tf_endresult_numerator.setEditable(false);
					tf_endresult_denominator.setEditable(true);
					tf_endresult_denominator.requestFocusInWindow();
				}else{
					lb_info.setText(
						String.valueOf(provisionalresult_numerator) +
						" - " +
						String.valueOf(result_integer) +
						" * "+
						String.valueOf(provisionalresult_denominator) +
						" = ?"
					);
					tf_endresult_numerator.setText("");
				}
			}
		}else if(obj == tf_endresult_denominator & key == KeyEvent.VK_ENTER){
			//System.out.println("focusLost (tf_endresult_denominator)");
			if(readInputInt(tf_endresult_denominator)!=null){
				int zz = Integer.parseInt(tf_endresult_denominator.getText());
				if(zz == provisionalresult_denominator){
					tf_endresult_denominator.setEditable(false);
					if(bl_randomProblem){points++;};
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "divideFractionsByFractions");
					lb_info.setText(lang.Messages.getString("that_is_right"));
					btn_continue.requestFocusInWindow();
				}else{
					lb_info.setText(lang.Messages.getString("num_stays_unchanged"));
					tf_endresult_denominator.setText("");
				}
			}
		}else if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}//if(obj ==..)
	}//keyPressed

	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}

	/**
	 * 
	 * @param n
	 * @param nn
	 * @return
	 */
	private boolean inRow(int n, int nn){
		boolean wert = false;
		if (nn>n){
			if(nn%n == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn<n){
			if(n%nn == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn == n){ wert =  true;}
		return wert;
	}//inRow(int n, int nn)
	
	/**
	 * 
	 *
	 */
	private void tf_reduce_outer_denominator_FocusLost(){
		//System.out.println("tf_reduce_outer_denominator_FocusLost");
		int z=0; int z_neu = 0; int n = 0; int n_neu = 0;
		boolean bol = true;
		try{
			z = Integer.parseInt(tf_reduce_inner_numerator.getText());
			z_neu = Integer.parseInt(tf_reduce_outer_numerator.getText());
			n = Integer.parseInt(tf_reduce_inner_denominator.getText());
			n_neu = Integer.parseInt(tf_reduce_outer_denominator.getText());
		}catch(NumberFormatException nfe){
			nfe.printStackTrace();
			lb_info.setText("error tf_reduce_outer_denominator_FocusLost");
			bol = false;
		}
		if(bol){
			if(z/n==z_neu/n_neu && inRow(z,z_neu) && inRow(n,n_neu)){
				if(JFractionLab.greatestCommonDivisor(z_neu,n_neu) != 1 || z/z_neu != n/n_neu){ //Es kann weitergekrzt werden
					lb_info.setText(lang.Messages.getString("reduce_better"));
					tf_reduce_outer_numerator.requestFocusInWindow();
				}else{//Es kann nicht weitergekrzt werden
					tf_reduce_outer_numerator.setEditable(false);
					tf_reduce_outer_denominator.setEditable(false);
					tf_reduce_outer_numerator.setText("");
					tf_reduce_outer_denominator.setText("");
					tf_reduce_inner_numerator.setText(String.valueOf(z_neu));
					tf_reduce_inner_denominator.setText(String.valueOf(n_neu));
					checkReducability();
				}//if (JFractionLab.ggt.JFractionLab.ggt(z,n) != 1)
			}else{//sind nicht in Reihe
				//System.out.println("Eine der Zahlen ist keine Vielfache!!");
				tf_reduce_outer_numerator.setText("");
				tf_reduce_outer_denominator.setText("");
				lb_info.setText(lang.Messages.getString("div_n_and_dn_by_same_nb"));
				tf_reduce_outer_numerator.requestFocusInWindow();
			}//if in reihe
		}
	}

}//class
