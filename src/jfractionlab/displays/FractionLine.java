/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.displays;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

import jfractionlab.JFractionLab;


public class FractionLine extends JComponent{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	public FractionLine(){
		setBackground(Color.white);
	}
	
    public void paintComponent(Graphics g){
		g.fillRect(0, getHeight()/2-1, getWidth(), 2);
    }
}