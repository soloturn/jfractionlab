/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.displays;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

import jfractionlab.JFractionLab;

public class FractionAsRectangle extends JComponent{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	    private int numerator1;
	    private int denominator1;
	    private int numerator2;
	    private int denominator2;
		private boolean twoFractionsOrdered = false;
		private boolean noView;
		private boolean horizontal;
		private boolean isQuadrate;
		private int shorterside;
		private Color color;
		Color backgroundOfPizza = Color.white;
		Color gridOfPizza = Color.black;

		public FractionAsRectangle(){
			noView = true;
		}//konstruktor bildklasse

		public FractionAsRectangle(
				int numerator1,
				int denominator1,
				Color color,
				boolean horizontal,
				boolean isQuadrate
		){
			drawPizzaAsRectangle(
					numerator1,
					denominator1,
					color,
					horizontal,
					isQuadrate
			);
		}//konstruktor bildklasse

		public FractionAsRectangle(
				int numerator1,
				int denominator1,
				int numerator2,
				int denominator2,
				Color color,
				boolean isQuadrate
		){
			drawPizzaAsRectangle(
					numerator1,
					denominator1,
					numerator2,
					denominator2,
					color,
					isQuadrate
			);
		}//konstruktor bildklasse

		public void drawPizzaAsRectangle(
				int numerator1,
				int denominator1,
				Color color,
				boolean horizontal,
				boolean isQuadrate
		){
			this.numerator1 = numerator1;
			this.denominator1 = denominator1;
			this.color = color;
			this.horizontal = horizontal;
			this.isQuadrate = isQuadrate;
			twoFractionsOrdered = false;
			noView = false;
			repaint();
		}//drawPizzaAsRectangle

		public void drawEmptyPizzaAsRectangle(
				int denominator,
				boolean horizontal,
				boolean isQuadrate
		){
			this.numerator1 = 0;
			this.denominator1 = denominator;
			this.color = Color.white;
			this.horizontal = horizontal;
			this.isQuadrate = isQuadrate;
			noView = false;
			repaint();
		}
		
		public void drawPizzaAsRectangle(
				int numerator1,
				int denominator1,
				int numerator2,
				int denominator2,
				Color color,
				boolean isQuadrate
		){
			this.numerator1 = numerator1;
			this.denominator1 = denominator1;
			this.numerator2 = numerator2;
			this.denominator2 = denominator2;
			this.color = color;
			this.isQuadrate = isQuadrate;
			this.horizontal = false;
			twoFractionsOrdered = true;
			noView = false;
			repaint();
		}//drawPizzaAsRectangle
		
		public void noPizzaAsRectangle(){
			noView = true;
			repaint();
		}	

		@Override
		public void paintComponent(Graphics g){
			Graphics2D screen2D = (Graphics2D)g;
			if(!noView){
				screen2D.setColor(color);
				getShorterSide();
				double links;
				double oben;
				if(isQuadrate){
					links = getWidth()/2 - (shorterside/2 - shorterside/20);
					oben = getHeight()/2 - (shorterside/2 - shorterside/20);
				}else{
					links = getWidth()/2 -(getWidth()/2 - getWidth()/40);
					oben = getHeight()/2 - (getHeight()/2 - getHeight()/40);
				}
				double breite = getWidth() - 2*links;
				double spalte = breite/denominator1;
				double hoehe = getHeight() - 2*oben;
				double zeile = hoehe;
				double neulinks = links;
				double neuoben = oben;
				screen2D.setColor(color);
				if(horizontal){
					zeile = hoehe/denominator1;
					neuoben = oben;
					for (int n = 0; n < numerator1; n++){
						if (n == 0){}else{neuoben = neuoben + zeile;}
						Rectangle2D.Double rcz = new Rectangle2D.Double(
							links, neuoben, spalte*denominator1, zeile
						);
						screen2D.fill(rcz);
					}//for
					neuoben = oben;
					screen2D.setColor(gridOfPizza);
					for (int m = 0; m < denominator1; m++){
						if (m == 0){}else{neuoben = neuoben + zeile;}
						Rectangle2D.Double rcz = new Rectangle2D.Double(
							links, neuoben, spalte*denominator1, zeile
						);
						screen2D.draw(rcz);
					}//for
				}else{
					for (int i = 0; i  < numerator1;  i ++){
						if (i == 0){}else{neulinks = neulinks + spalte;}
						Rectangle2D.Double rcz = new Rectangle2D.Double(neulinks, oben, spalte, hoehe);
						screen2D.fill(rcz);
					}//for
					if (twoFractionsOrdered){
						zeile = hoehe/denominator2;
						//double neuoben = oben;
						//Farbe von Zaehler 1 loeschen
						neulinks = links;
						screen2D.setColor(backgroundOfPizza);
						for (int i = 0; i  < numerator1;  i ++){
							if (i == 0){}else{neulinks = neulinks + spalte;}
							//neuebreite = neuebreite - breite/denominator1;
							Rectangle2D.Double rcz = new Rectangle2D.Double(neulinks, oben, spalte, hoehe);
							screen2D.fill(rcz);
						}//for
						//Zaehler des zweiten Bruches (gefuellte Flaeche)
						screen2D.setColor(color);
						for (int n = 0; n < numerator2; n++){
							if (n == 0){}else{neuoben = neuoben + zeile;}
							Rectangle2D.Double rcz = new Rectangle2D.Double(
								links, neuoben, spalte*numerator1, zeile
							);
							screen2D.fill(rcz);
						}
						//Nenner 2 (Gitter des zweiten Bruches)
						neuoben = oben;
						screen2D.setColor(gridOfPizza);
						for (int i = 0; i  < denominator2;  i ++){
							if (i == 0){}else{neuoben = neuoben + zeile;}
							Rectangle2D.Double rcz = new Rectangle2D.Double(links, neuoben, spalte*denominator1, zeile);
							hoehe = zeile * denominator2;//wegen rundungsdifferenzen muss hoehe neu gesetzt werden
							screen2D.draw(rcz);
						}//for
					}//if zweitfraction
					//denominator1 (Gitter des ersten Bruches)
					neulinks = links;
					for (int i = 0; i  < denominator1;  i ++){
						screen2D.setColor(gridOfPizza);
						if (i == 0){}else{neulinks = neulinks + spalte;}
						Rectangle2D.Double rcx = new Rectangle2D.Double(neulinks, oben, spalte, hoehe);
						screen2D.draw(rcx);
					}//for
				}//horizontal
			}else{ 
				screen2D.setBackground(Color.white);
			}
		}//paint

		private void getShorterSide(){
			if (getHeight() < getWidth()){
				shorterside = getHeight();
			}else{
				shorterside = getWidth();
			}//if
		}//getKleineSeite

}//PizzaAsRectangleklasse