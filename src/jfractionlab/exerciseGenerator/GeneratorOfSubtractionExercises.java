package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;
import jfractionlab.JFractionLab;

public class GeneratorOfSubtractionExercises extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int m1,
			int m2
	){
		m1 = 10;
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int n2;
		int d2;
		int lcd;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
		
		//generate numbers
		do{
			FractionMaker fraction = new FractionMaker(m1);
			n1 = fraction.getNumerator_1();
			d1 = fraction.getDenominator_1();
			n2 = fraction.getNumerator_2();
			d2 = fraction.getDenominator_2();
		}while(!((float)n1/d1 > (float)n2/d2));
		lcd = JFractionLab.leastComonMultiple(d1, d2);
		int f1 = lcd/d1;
		int f2 = lcd/d2; 
		
		//testnumbers
//		n1 = 5; d1 = 6; n2 = 4; d2 = 6;			// result < 1; d1 == d2
//		n1 = 6; d1 = 7; n2 = 6; d2 = 14;			// result < 1; d1 != d2
//		n1 = 5; d1 = 10; n2 = 2; d2 = 9;	//result < 1; result is reducable
//		n1 = 6; d1 = 7; n2 = 3; d2 = 7;			//result < 1; result is not reducable
//		n1 = 5; d1 = 6; n2 = 1; d2 = 6;
//		lcd = JFractionLab.leastComonMultiple(d1, d2);
//		f1 = lcd/d1;
//		f2 = lcd/d2; 
		//testnumbers end
		
		//generate text of exercise and add it to the arraylist
			strExercise = "{"+n1+"}over{"+d1+"} - {"+n2+"}over{"+d2+"}";
			ex.getAlExercises().add(strExercise+" ={}");
		
		//generate text of quicksolution and add it to the arraylist
			strQuickSolution =  writeReducingTextResult(
					false,
					((n1*f1)-(n2*f2)),
					lcd
			);
			ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
			String rw = "";
			//write something like this:
			//{3*8}over{7*8} + {5*7}over{8*7} = {24}over{56} + {35}over{56} 
			if(f1 > 1 || f2 > 1){
				if(f1 > 1){
					rw += "{"+n1+" cdot "+f1+"}over{"+d1+" cdot "+f1+"}";
				}else{
					rw += "{"+n1+"}over{"+d1+"}";
				}
				rw += " - ";
				if(f2 > 1){
					rw += "{"+n2+" cdot "+f2+"}over{"+d2+" cdot "+f2+"}";
				}else{
					rw += "{"+n2+"}over{"+d2+"}";
				}
				rw+= " = ";
				rw += "{"+(n1*f1)+"}over{"+lcd+"} - {"+(n2*f2)+"}over{"+lcd+"}";
				rw += " = ";
			}
			
			//write something like this:
			//{59}over{56}
			rw += "{"+(n1*f1-n2*f2)+"}over{"+lcd+"}";

			//reduce
			rw += writeReducingTextResult(true, ((n1*f1)-(n2*f2)), lcd);
			strLongSolution = rw;

			ex.getAlCalculations().add(
					strExercise+
					" = "+
					strLongSolution
			);
		return ex;
	}
}
