package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;

public class GeneratorOfExtendFractionsExercises extends GeneratorObject{

	@Override
	public Exercises getOneExercise(
			int range1,
			int range2
	){
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int factor;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
			
		//generate numbers
		FractionMaker fraction = new FractionMaker(range1);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();
		factor = Math.abs(jfractionlab.JFractionLab.ran.nextInt(range2))+1;
		
		//generate text of exercise and add it to the arraylist
		strExercise = "{"+n1+"} over {"+d1+"} = csup{"+factor+"}";
		ex.getAlExercises().add(strExercise+" {}");

		//generate text of quicksolution and add it to the arraylist
		strQuickSolution = "{"+(n1*factor)+"} over {"+(d1*factor)+"}";
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		strLongSolution = "{"+n1+" cdot "+factor+"} over {"+d1+" cdot "+factor+"}";
		ex.getAlCalculations().add(
				strExercise+
				strLongSolution+
				" = "+
				strQuickSolution
		);
		return ex;
	}
}
