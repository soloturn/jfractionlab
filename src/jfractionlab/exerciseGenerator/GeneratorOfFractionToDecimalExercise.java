
package jfractionlab.exerciseGenerator;

import java.text.NumberFormat;

import jfractionlab.FractionMaker;

public class GeneratorOfFractionToDecimalExercise extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int range1,
			int range2
	){

		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int factor;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
			
		//generate numbers
		FractionMaker fraction = new FractionMaker();
		fraction.mkDecimalFraction(true);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();
		factor = calculateDenominator2(d1)/d1;

		//generate text of exercise and add it to the arraylist
		strExercise = "{"+n1+"} over {"+d1+"} ";
		ex.getAlExercises().add(strExercise+" = {}");

		//generate text of quicksolution and add it to the arraylist

		NumberFormat form;
		form = NumberFormat.getInstance(lang.Messages.getLocale());
		strQuickSolution = String.valueOf(
				form.format(n1/(float)d1)
		);
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		strLongSolution =  " = csup{"+factor+"} {"+(n1*factor)+" over "+(d1*factor)+"}";
		
		//add exercise
		ex.getAlCalculations().add(
				strExercise +
				strLongSolution +
				" = "+
				strQuickSolution
		);
		return ex;
	}
	
	private int calculateDenominator2(int dn1){
		if(10%dn1==0){
			return 10;
		}else{
			return 100;
		}
	}
}
