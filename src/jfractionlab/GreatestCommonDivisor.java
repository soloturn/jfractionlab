package jfractionlab;


//GreatestCommonDivisor is used by jfractionlab.worksheets.WSGMultiplication
//to calculate the best reducing-operations

public class GreatestCommonDivisor implements Comparable<GreatestCommonDivisor>{

	private String name;
	private int gcd;
	
	
	public GreatestCommonDivisor(String name, int gcd){
		this.name = name;
		this.gcd = gcd;
	}

	@Override
	public int compareTo(GreatestCommonDivisor obj) {
//		obj is bigger	--> return -x
//		obj is equal	--> return 0
//		obj is smaller	--> return x
		int i=0;
		if(obj.getGcd() > this.getGcd()){
			i = -1;
		}
		if(obj.getGcd() == this.getGcd()){
			i = 0;
		}
		if(obj.getGcd() < this.getGcd()){
			i = 1;
		}
		return i;
	}
	
	public String getName() {
		return name;
	}


	public int getGcd() {
		return gcd;
	}


	public void setName(String name) {
		this.name = name;
	}


	public void setGcd(int gcd) {
		this.gcd = gcd;
	}

}
