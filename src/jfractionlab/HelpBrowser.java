package jfractionlab;

import info.clearthought.layout.TableLayout;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class HelpBrowser extends JFrame implements ActionListener, HyperlinkListener {
	static final long serialVersionUID =jfractionlab.JFractionLab.serialVersionUID;
	private MyAntiAliasedJEditorPane jep_browser = new MyAntiAliasedJEditorPane();
	private JButton btn_end;

	public HelpBrowser(String lang, String startpoint){
		super();
		if (lang.startsWith("de")){
			//siehe google:"java open browser"
			//using the desktop api in java
			//java.awt.Desktop
			createAndShowGUI("/"+startpoint+"_de.html");
		}else if (lang.startsWith("en")){
			createAndShowGUI("/"+startpoint+"_en.html");
		}else if (lang.startsWith("fr")){
			createAndShowGUI("/"+startpoint+"_fr.html");
		}else if (lang.startsWith("pt")){
			createAndShowGUI("/"+startpoint+"_pt.html");
		}else if (lang.startsWith("es")){
			createAndShowGUI("/"+startpoint+"_es.html");
		}else{
			createAndShowGUI("/"+startpoint+"_en.html");
		}
		//TODO add greek
	}
	
	private void createAndShowGUI(String page){
		setTitle("JFractionLab - " +lang.Messages.getString("jmi_help"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		double insets = 0.01;
		double sizes[][] = {{
			//Spalten
			insets,
			TableLayout.FILL,
			insets
		},{
			//Zeilen
			insets,
			TableLayout.FILL,
			insets,
			0.05,
			insets
		}};
		Container cp = getContentPane();
		cp.setLayout(new TableLayout(sizes));
			jep_browser.setContentType("text/html");
			jep_browser.addHyperlinkListener(this);
			jep_browser.setEditable(false);
			try {
				//the "index-file" has to be in a jar-file
				//which is in the BuildPath
//				JOptionPane.showMessageDialog(null, 
//						getClass().getResource(page).toString()+ " helpbrowser startseite");
				jep_browser.setPage(
					getClass().getResource(page)
				);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				jep_browser.setText(repairHelpInfo());
			} catch (IOException e) {
				e.printStackTrace();
				jep_browser.setText(repairHelpInfo());
			}
			JScrollPane sp = new JScrollPane(jep_browser);
		cp.add(sp, "1,1");
			btn_end = new JButton(lang.Messages.getString("close"));
			btn_end.addActionListener(this);
		cp.add(btn_end, "1,3,c,f");
		setLocation(100,100);
		setSize(950, 550);
		setResizable(true);
		try{
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			//UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			//UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		}catch(Exception exc){
			exc.printStackTrace();
			//System.out.println("UIManager Problem");
		}
		setVisible(true);	
	}
	
	public String repairHelpInfo(){
		String str ="";
		str = JFractionLab.jep_fontface + "<b>"
		+ lang.Messages.getString("Help_jar_is_missing")
		+ "<br><br>"
		+ lang.Messages.getString("download_Help_jar")
		+ "<br><br>"
		+ lang.Messages.getString("install_Help_jar")
		+ "<br><br>"
		+ lang.Messages.getString("install_Help_jar_tip")
		+ "</b></font>";
		
		return str;
	}
	
	public void hyperlinkUpdate( HyperlinkEvent event ){
		HyperlinkEvent.EventType typ = event.getEventType();
		if ( typ == HyperlinkEvent.EventType.ACTIVATED ){
			try{
				jep_browser.setPage( event.getURL() ); 
			}catch( IOException e ) { 
				JOptionPane.showMessageDialog( this, 
					"Can't follow link to " 
					+ event.getURL().toExternalForm(), 
					"Error", 
					JOptionPane.ERROR_MESSAGE
				); 
			}//catch
		}//if
	}//hyperlinkUpdate
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_end){
			setVisible(false);
			dispose();
		}
	}//actionPerformed
}//class