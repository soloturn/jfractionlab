/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import jfractionlab.ConfManager;
import jfractionlab.JFractionLab;

public class UsabilityDialog extends JDialog 
		implements ActionListener, ItemListener
{	
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	public static final int USABILITYTIPP =0;
	public static final int OFFICEHINT =1;
	private Container content;
	private JEditorPane jep_info = new JEditorPane();
	private JButton btn_OK = new JButton("");
	private JCheckBox cb_tipp = new JCheckBox("", true);
	
	public UsabilityDialog(String[] ar_howto){ 
		if(new ConfManager().isTipAtStart()){
			showUsabilityDialog(ar_howto);
		}
	}//Konstruktor

	
	private void showUsabilityDialog(String[] ar_howto){
//		System.out.println("==showUsabilityDialog");
		
		setTitle(lang.Messages.getString("howto"));
		double sizes[][] = {{
			5,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			0.1,
			0.1,
			5
		}}; //Spalten / Zeilen
		getRootPane().setDefaultButton(btn_OK);
		content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
			jep_info.setContentType("text/html");
			String howtotxt = JFractionLab.jep_fontface + "<b>";
			for(int nb = 0; nb < ar_howto.length; nb++){
				howtotxt = howtotxt + lang.Messages.getString(ar_howto[nb]) + "<br>";
			}
			howtotxt = howtotxt +"</b></font>";
			jep_info.setText(howtotxt);
			jep_info.setEditable(false);
			jep_info.setFocusable(false);
		content.add(new JScrollPane(jep_info), "1,1");
			cb_tipp.setText(lang.Messages.getString("show_hints_on_start"));
			cb_tipp.setBackground(Color.white);
			cb_tipp.addItemListener(this);
			cb_tipp.setFocusable(false);
		content.add(cb_tipp, "1,2,f,c");
			btn_OK.addActionListener(this);
			btn_OK.setFocusTraversalKeysEnabled(false);
			btn_OK.setText(lang.Messages.getString("OK"));
		content.add(btn_OK, "1,3,c,c");
		
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setLocation(150, 150);
		setSize(600,300);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);	
	}//showUsabilityDialog

	@Override
	public void actionPerformed(ActionEvent e){
		Object obj = e.getSource();
		if (obj == btn_OK){
			if(!cb_tipp.isSelected() ){
				new ConfManager().setTipAtStart(false);
			}
			setVisible(false);
			dispose();
		}
	}//actionPerformed

	@Override
	public void itemStateChanged(ItemEvent e){
		//System.out.println("==itemStateChanged");
		Object obj = e.getSource();
		if (obj == cb_tipp){
			if(!cb_tipp.isSelected()){
				JFractionLab.cb_showtippatstart.setSelected(false);
			}
		}
	}//itemStateChanged

}//class UsabilityDialog
