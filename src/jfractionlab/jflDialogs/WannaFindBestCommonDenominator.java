package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import jfractionlab.JFractionLab;
import jfractionlab.exerciseDialogs.ExerciseDialog;

public class WannaFindBestCommonDenominator extends JDialog implements ActionListener{
	
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	private Container content;
	private JEditorPane jep_text = new JEditorPane();
	private JButton btn_yes = new JButton("");
	private JButton btn_no = new JButton("");
	public ExerciseDialog owner;
	MyOwnFocusTraversalPolicy newPolicy;
	
	public WannaFindBestCommonDenominator(ExerciseDialog owner){
		this.owner = owner;
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		newPolicy = new MyOwnFocusTraversalPolicy();
		createGUI();
	}
	
	private void createGUI(){
		double sizes[][] = {{
			5,
			TableLayout.FILL,
			5,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			5,
			0.25,
			5
		}}; //Spalten / Zeilen
		getRootPane().setDefaultButton(btn_yes);
		content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
			jep_text.setContentType("text/html");
			jep_text.setText(
					JFractionLab.jep_fontface
					+ "<b>"
					+ lang.Messages.getString("continue_or_find_best_common_denominator")
					+ "</b></font>"
			);
			jep_text.setEditable(false);
		content.add(new JScrollPane(jep_text), "1,1,3,1");
			btn_no.setText(lang.Messages.getString("continue_with_actual_common_denominator"));
			btn_no.addActionListener(this);
		content.add(btn_no, "1,3");
			btn_yes.setText(lang.Messages.getString("find_best_common_denominator"));
			btn_yes.addActionListener(this);
		content.add(btn_yes, "3,3");

		setFocusTraversalPolicy(newPolicy);
		setLocation(150, 150);
		setSize(500,150);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);	
	}//showUsabilityDialog
	
	private void actionYES(){
		owner.bl_WannaFindBestCommonDenominator_AnswerIsYes = true;
		close();
	}
	
	private void actionNO(){
		close();
	}
	
	private void close(){
		setVisible(false);
		dispose();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_no){
			actionNO();
		}else if (obj == btn_yes){
			actionYES();
		}
	}
	
	public class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
		@Override
		public Component getComponentAfter(Container aContainer,Component aComponent) {
			//soll: btn_yes, btn_no,
			if(aComponent.equals(btn_yes)) {
				return btn_no;
			}else if(aComponent.equals(btn_no)){
				return btn_yes;
			}else{
				return btn_yes;
			}
		}

		@Override
		public Component getComponentBefore(Container aContainer,Component aComponent) {
			//soll: btn_yes, btn_no
			if(aComponent.equals(btn_yes)) {
				return btn_no;
			}else if(aComponent.equals(btn_no)){
				return btn_yes;
			}else{
				return btn_yes;
			}
		}

		@Override
		public Component getDefaultComponent(Container aContainer) {
			return btn_yes;
		}

		@Override
		public Component getFirstComponent(Container aContainer) {
			return btn_yes;
		}

		@Override
		public Component getLastComponent(Container aContainer) {return null;}
	}//MyOwnFocusTraversalPolicy
}
