/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.logtable;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import jfractionlab.JFractionLab;

public class LogTable extends JFrame implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		//GUI
		private JLabel lb_titel = new JLabel("", JLabel.CENTER);
		private Container content;
		private JTable table = new JTable();
		private JScrollPane tableScrollPane = new JScrollPane(table);
		private JButton btn_save = new JButton("");
		private JButton btn_close = new JButton("");
		private MyTableModel mtm;
		private String[] colnames;
		/**
		 * 
		 * @param owner
		 * @param data
		 */
		public LogTable(JFractionLab owner, String[][] data, String[] colnames){
//			System.out.println("data.length = "+data.length);
//			System.out.println("data[0].length = "+data[0].length);
			setTitle(lang.Messages.getString("show_logs"));
			this.colnames = colnames;
			
			lb_titel.setText(lang.Messages.getString("survey_results"));
			btn_save.setText(lang.Messages.getString("save"));
			btn_close.setText(lang.Messages.getString("end"));
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			setSize(670,700);
			setLocation(60,60);
			double sizes[][] = {{
				// Spalten
				0.025,
				TableLayout.FILL,
				TableLayout.FILL,
				TableLayout.FILL,
				0.025
			},{
				//Zeilen
				0.025,
				30,
				TableLayout.FILL,
				30,
				0.025
			}};
			content = getContentPane();
			content.setLayout(new TableLayout(sizes));
			content.setBackground(Color.white);
			content.add(lb_titel, "1,1,3,1,c,c");
				mtm = new MyTableModel(data, colnames);
				table.setModel(mtm);
				TableColumn column = null;
				TableCellRenderer headerRenderer = new VerticalTableHeaderCellRenderer();
				for (int i = 0; i < table.getColumnCount(); i++) {
					column = table.getColumnModel().getColumn(i);
					if (i == 0) {
						column.setMinWidth(100);
					}else if (i == 1) {
						column.setMinWidth(130);
					}else if (i > 1){
						column.setPreferredWidth(30);
						column.setHeaderRenderer(headerRenderer);
					}
				}//for
			content.add(tableScrollPane, "1,2,3,2");
				btn_save.addActionListener(this);
			content.add(btn_save, "1,3,c,c");
				btn_close.addActionListener(this);
			content.add(btn_close, "3,3,c,c");
			setVisible(true);
		}//Konstruktor

		/**
		 * 
		 */
		public void actionPerformed (ActionEvent e) {
			Object obj = e.getSource();
	        	if (obj == btn_close){
				setVisible(false);
				dispose();
			}else if (obj == btn_save){
				String outputstring ="";
				for (int i = 0; i < colnames.length - 1; i++){
					outputstring = outputstring + "\"" +colnames[i] + "\",";
				}
				//hinter dem letzten Eintrag soll kein Komma!
				outputstring = outputstring + "\"" + colnames[colnames.length -1] + "\"\n";
				for(int i = 0; i < mtm.data.length; i++){
					for(int cl = 0; cl<colnames.length-1; cl++){//uzt
						outputstring = outputstring+"\""+mtm.data[i][cl]+"\",";
					}
					outputstring = outputstring+"\""+mtm.data[i][colnames.length-1]+ "\"\n";//uzt
				}//for
				outputstring = outputstring+"\n";
				//System.out.println(outputstring);
				JFileChooser fc = new JFileChooser();
				FileFilter myfilter = new FileNameExtensionFilter("comma separated value", "csv", "CSV");
				fc.addChoosableFileFilter(myfilter);
				fc.setLocale(lang.Messages.getLocale());
				int returnVal = fc.showSaveDialog(LogTable.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try{
						FileWriter fw = new FileWriter(file);
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(outputstring);
						bw.close();
						fw.close();
					}catch (IOException ex) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(null,"Error! Can't write the file.");
					}//acatch
				}//if
			}//if (obj==btn_save)
		}//actionPerformed
	}//class
