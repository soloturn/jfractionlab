package jfractionlab;

import java.io.Serializable;

public class ConfSettings implements Serializable{
	
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	private boolean tipAtStart;
	private String[] unoConfJars = new String[JFractionLab.nbOfUnoConfJars];
	
	public ConfSettings(){
		this.tipAtStart = true;
		for(int i = 0; i<unoConfJars.length;i++){
			unoConfJars[i] = "";
		}
	}

	public ConfSettings(
			boolean tip,
			String[] unoConfJars,
			String jurtPath,
			String unoilPath,
			String sofficePath
		){
		this.tipAtStart = tip;
		unoConfJars = new String[JFractionLab.nbOfUnoConfJars];
		this.unoConfJars = unoConfJars;
	}
	
	public boolean isTipAtStart() {return tipAtStart;}
	public void setTipAtStart(boolean bl) {this.tipAtStart = bl;}
		
	public String[] getUnoConfJars() {return unoConfJars;}
	public void setUnoConfJars(String[] str) {this.unoConfJars = str;}

	@Override
	public String toString(){
		String str = "tipAtStart = "+ String.valueOf(tipAtStart);
		str += ";\n pathToLO = \n";
		for (int i = 0; i < unoConfJars.length; i++){
			str += unoConfJars[i] +";\n";
		}
		return str;
	}
}
