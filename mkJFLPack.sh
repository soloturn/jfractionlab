#!/bin/sh

#This script compiles JFractionLab on linux
#copy it to your home-directory
#go to your home-directory and run it like:
# ./mkJFLPack.sh <versinNB>

getSRC(){
	echo "getSRC()"
	mkdir "$WORKSPACE"
	echo "src = ""$SRCDIR"
	cp -a "$SRCDIR"/* "$WORKSPACE"
	rm -rf "$WORKSPACE"/bin/*
	rm -rf "$WORKSPACE"/libs/Help*
}
packSRC(){
	ZIPDIR="jflsrc_""$VERSION"
	mkdir $ZIPDIR
	mv "$WORKSPACE"/* "$ZIPDIR"
	zip -r JFractionLab_"$VERSION"_src.zip "$ZIPDIR"
	rm -rf "$WORKSPACE"
	rm -rf "$ZIPDIR"
}
setVersionNB(){
	echo "setVersionNB()"
	# make some changes in the code
	cd "$WORKSPACE"/src/jfractionlab/
	#set Version Number
	OLD_VN=`grep JFractionLab_version-number_lab JFractionLab.java`
	NEW_VN="setTitle(\"JFractionLab_"$VERSION"\");"
	sed s/"$OLD_VN"/"$NEW_VN"/g JFractionLab.java > jfl.p
	mv jfl.p JFractionLab.java
	cd "$WORKSPACE"
}
compile(){
	echo "compile()""-- $WORKSPACE"
	UNO=/opt/libreoffice3.4/ure/share/java/
	UNOIL=/opt/libreoffice3.4/basis3.4/program/classes/
	LO=/opt/libreoffice3.4/program/
	export CLASSPATH="$CLASSPATH":.:`pwd`/libs/*:`pwd`/bin/*:`pwd`/src/*:"$UNO"java_uno.jar:"$UNO"juh.jar:"$UNO"jurt.jar:"$UNO"ridl.jar:"$UNO"unoloader.jar:"$UNOIL"unoil.jar:"$LO"soffice
	cd src/
	"$JAVAC" -client jfractionlab/JFractionLab.java
	"$JAVAC" -client lang/Messages.java
	cd "$WORKSPACE"
}
build(){
	echo "build()"
	# clean up a little bit
	rm src/jfractionlab/*java
	rm src/jfractionlab/*/*java
	mv src/jfractionlab bin
	rm src/lang/*java
	mv src/lang/ bin
	#make help
		cd doc/help_de
		"$JAR" cf Help_de.jar *
		cp Help_de.jar ../../libs
		cd ../..
	#help is done
	#make english help
		cd doc/help_en
		"$JAR" cf Help_en.jar *
		cp Help_en.jar ../../libs
		cd ../..
	#english help is done
	#make spanish help
		cd doc/help_es
		"$JAR" cf Help_es.jar *
		cp Help_es.jar ../../libs
		cd ../..
	#spanish help is done
	#make french help
		cd doc/help_fr
		"$JAR" cf Help_fr.jar *
		cp Help_fr.jar ../../libs
		cd ../..
	#french help is done
	#make portuguese help
		cd doc/help_pt
		"$JAR" cf Help_pt.jar *
		cp Help_pt.jar ../../libs
		cd ../..
	#portuguese help is done
	rm bin/lang/jflTextResource.txt
	rm -rf src doc
	mv libs/*jar bin
	rm -rf libs
	cp -a icons bin
	cd bin
		
	# make a jar-file
	#create the manifest
	echo "Main-Class: jfractionlab/JFractionLab" >> myManifest
	echo -n "Class-Path: TableLayout-bin-jdk1.5-2009-06-10.jar " >> myManifest
	echo "Help_de.jar Help_en.jar Help_es.jar Help_fr.jar Help_pt.jar lang" >> myManifest
	#echo -n "/opt/libreoffice3.4/ure/share/java/java_uno.jar " >> myManifest
	#echo -n "/opt/libreoffice3.4/ure/share/java/juh.jar " >> myManifest
	#echo -n "/opt/libreoffice3.4/ure/share/java/jurt.jar " >> myManifest
	#echo -n "/opt/libreoffice3.4/ure/share/java/ridl.jar " >> myManifest
	#echo -n "/opt/libreoffice3.4/ure/share/java/unoloader.jar " >> myManifest
	#echo  "/opt/libreoffice3.4/basis3.4/program/classes/unoil.jar " >> myManifest

	# option splash 
	echo "Splashscreen-Image: icons/splash.png" >> myManifest
	echo "Sealed: true" >> myManifest
	"$JAR" cmf myManifest JFractionLab.jar icons/ jfractionlab/ lang/
	#clean up
	rm -rf icons/ jfractionlab/ lang/ myManifest
	cd "$WORKSPACE"
}
mkIzPackJar(){
	echo "mkIzPackJar()"
	#to use this you have to install IzPack
	#IzPack uses config files, you find them in the directory mkInstaller
	#you have to edit all the paths in the izpack config files!
	JFRACTIONLAB="jfractionlab"
	mkdir ~/"$JFRACTIONLAB"
	mv bin CHANGELOG.txt icons templates LICENCE.txt XInfoPanelTxt.txt ~/"$JFRACTIONLAB"
	cd "$WORKSPACE"/mkInstaller
	JARNAME=JFractionLab_"$VERSION".jar
	#mk JAR
	/usr/local/bin/IzPack/bin/compile mk_jfl_installer.xml -b . -o "$JARNAME" -k standard
	#mk EXE
python /usr/local/bin/IzPack/utils/wrappers/izpack2exe/izpack2exe.py --file="$JARNAME" --output=JFractionLab_"$VERSION".exe
#	mv "$JARNAME" *exe ~
#	cd ~
}
####################################################################
if [ $# -ne 2 ]; then
	echo "Please give me the versionnumber and the path to the source: ./mkJFLPack.sh 024 foo/jflsrc_024/"
else
	JAVA=`which java`
	JAVAC=`which javac`
	JAR=`which jar`
	WORKSPACE=`pwd`/"jflworkspace"
	VERSION="$1"
	SRCDIR="$2"
	
	echo ""
	echo "JAVA      = " $JAVA
	echo "JAVAC     = " $JAVAC
	echo "JAR       = " $JAR
	echo "WORKSPACE = " $WORKSPACE
	echo "VERSION   = " $VERSION
	echo ""

	# make a source zip
		getSRC
		packSRC

	# compile
		getSRC
		setVersionNB
		compile
		build
		mkIzPackJar
		
		rm -rf "$WORKSPACE"
		rm -rf "$JFRACTIONLAB"
fi
######################################################################

